package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	imgbuff, err := ioutil.ReadFile("logo.jpg")
	if err != nil {
		fmt.Println("can not read file, or file not found")
	}

	convImgPath, err := img2tiff(imgbuff, 60)
	if err != nil {
		fmt.Println("something went wrong while processing : ", err.Error())
		return
	}

	fmt.Println("finished! : ", convImgPath)
}
