package main

import (
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/h2non/bimg"
)

func img2webp(buffer []byte, quality int) (string, error) {

	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".webp"

	converted, err := bimg.NewImage(buffer).Convert(bimg.WEBP)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(fmt.Sprintf("./conv/%s", filename), processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

func img2png(buffer []byte, quality int) (string, error) {

	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".png"

	converted, err := bimg.NewImage(buffer).Convert(bimg.PNG)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(fmt.Sprintf("./conv/%s", filename), processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

func img2jpg(buffer []byte, quality int) (string, error) {

	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".jpg"

	converted, err := bimg.NewImage(buffer).Convert(bimg.JPEG)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(fmt.Sprintf("./conv/%s", filename), processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

func img2gif(buffer []byte, quality int) (string, error) {

	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".gif"

	converted, err := bimg.NewImage(buffer).Convert(bimg.GIF)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(fmt.Sprintf("./conv/%s", filename), processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

func img2tiff(buffer []byte, quality int) (string, error) {

	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".tif"

	converted, err := bimg.NewImage(buffer).Convert(bimg.TIFF)
	if err != nil {
		return filename, err
	}

	processed, err := bimg.NewImage(converted).Process(bimg.Options{Quality: quality})
	if err != nil {
		return filename, err
	}

	writeError := bimg.Write(fmt.Sprintf("./conv/%s", filename), processed)
	if writeError != nil {
		return filename, writeError
	}

	return filename, nil
}

//func img2heif(buffer []byte, quality int) (string, error) {}
//func img2pdf(buffer []byte, quality int) (string, error) {}
