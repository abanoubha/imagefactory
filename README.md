# imagefactory
image factory : optimizer / cleaner / compressor / converter

## TODO

- [x] img to jpg
- [x] img to jpeg
- [x] img to png
- [x] img to webp
- [ ] clean svg : [svgo](https://github.com/svg/svgo) , [svgcleaner](https://github.com/RazrFalcon/svgcleaner)
- [ ] compress jpg / jpeg lossless : [imageOptim](https://github.com/ImageOptim/ImageOptim)
- [ ] compress png lossless
- [ ] compress jpg / jpeg lossy
- [ ] convert jpg to webp
- [ ] convert png to webp
- [ ] convert png to jpg
- [ ] make GUI via [gotk3](https://github.com/gotk3/gotk3) or [gioui](https://github.com/gioui/gio)
- [ ] parallel processes in GUI : like [svgcleaner-gui](https://github.com/RazrFalcon/svgcleaner-gui)
- [ ] compress gif
- [ ] convert gif to jpg's
- [ ] convert gif to webp's
- [ ] convert pdf pages to png
- [ ] convert pdf pages to jpg
- [ ] convert pdf pages to webp
- [ ] convert images to pdf
- [ ] ai enlarge image

## TECH

- language : [Go](https://golang.org/)
- libs : [libvips](https://github.com/libvips/libvips), [bimg](https://github.com/h2non/bimg)
- releases : flatpak, snap, windows, android, apple (swiftui: mac, ios, ipados)

## Supported OS
- [ ] GNU / Linux
- [ ] Android
