module github.com/abanoubhannaazer/imagefactory

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/h2non/bimg v1.1.5
)
